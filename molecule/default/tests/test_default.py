import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_directories(host):
    dirs = [
            "/etc/confd",
            "/etc/confd/conf.d",
            "/etc/confd/templates"
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory
        assert d.exists


def test_files(host):
    files = [
            "/etc/matrix-synapse/default-env",
            "/etc/confd/templates/homeserver.yaml.tmpl",
            "/etc/confd/templates/etc_default_matrix-synapse.tmpl",
            "/etc/confd/conf.d/homeserver.toml",
            "/etc/confd/conf.d/etc_default_matrix-synapse.toml",
            "/etc/confd/templates/homeserver.signing.key.tmpl",
            "/etc/confd/conf.d/homeserver.signing.key.toml",
            "/etc/confd/templates/homeserver.tls.crt.tmpl",
            "/etc/confd/conf.d/homeserver.tls.crt.toml"

    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file

    assert host.file(
            "/etc/matrix-synapse/default-env").user == "matrix-synapse"

    assert host.file("/etc/matrix-synapse/log.yaml") \
        .contains("TimedRotatingFileHandler")


def test_service(host):
    s = host.service("matrix-synapse")
    assert not s.is_running
    # This is not working..
    # assert s.is_enabled


def test_socket(host):
    sockets = [
    ]
    for socket in sockets:
        s = host.socket(socket)
        assert s.is_listening


def test_systemd_service(host):
    unit = "/lib/systemd/system/matrix-synapse.service"
    f = host.file(unit)
    assert f.exists
    assert f.contains(r"^EnvironmentFile=/etc/matrix-synapse/default-env")
    assert f.contains(r"^EnvironmentFile=/etc/aws-environment")
    assert f.contains(r"^ExecStartPre=/usr/local/bin/confd.*")
    assert not f.contains(r"^ExecStartPre=.*synapse.app.homeserver.*")
    assert not f.contains(r".*generate-keys.*")
