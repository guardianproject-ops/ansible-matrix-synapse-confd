# ansible-matrix-synapse-confd

An Ansible role that installs [synapse](https://github.com/matrix-org/synapse/)
on a Debian system from official matrix.org repos and configures
[confd](https://github.com/kelseyhightower/confd) (not installed, see
[ansible-confd](https://gitlab.com/guardianproject-ops/ansible-confd)) to
configure synapse at runtime using the [AWS SSM Parameter
Store](https://docs.aws.amazon.com/systems-manager/latest/userguide/systems-manager-paramstore.html)

You can configure which package version to pin to, and whether to use python2
or 3.

**Warning**: This role makes no attempt to handle upgrades. The intention is
that you use this role to bake a fresh image. When you want to upgrade you make
a new image and apply this role fresh.

# Requirements

* Debian

# Role Variables

### Required Variables

```
# the region where you are deploying
confd_ssm_region: 'eu-central-1'
```

### Default Variables

See [defaults/main.yml](defaults/main.yml)

# Dependencies

none

# Example Playbook

    - hosts: servers
      roles:
         - { role: ansible-matrix-synapse-confd }

# Testing

```
molecule test --all
```

# License

[GNU Affero General Public License (AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html)

# Author Information

* Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)

